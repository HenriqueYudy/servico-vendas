package com.vendas.vendas.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "venda")
public class Vendas {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	@Column(name = "cliente")
	private String cliente;

	@Column(name = "funcionario")
	private long funcionario;

	@Column(name = "jogo")
	private long jogo;

	@Column(name = "valor")
	private BigDecimal valor;

	@Column(name = "quantidade")
	private int quantidade;

	@Column(name = "valor_total")
	private BigDecimal valor_total;



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public long getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(long funcionario) {
		this.funcionario = funcionario;
	}

	public long getJogo() {
		return jogo;
	}

	public void setJogo(long jogo) {
		this.jogo = jogo;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getValor_total() {
		return valor_total;
	}

	public void setValor_total(BigDecimal valor_total) {
		this.valor_total = valor_total;
	}
	
	


	@Override
	public String toString() {
		return "Vendas [id=" + id + ", cliente=" + cliente + ", funcionario=" + funcionario + ", jogo=" + jogo
				+ ", valor=" + valor + ", quantidade=" + quantidade + ", valor_total=" + valor_total + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vendas other = (Vendas) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
